var exec = require('cordova/exec');

exports.showModal = function (arg0, success, error) {
    exec(success, error, 'AvaloqModal', 'showModal', [arg0]);
};
