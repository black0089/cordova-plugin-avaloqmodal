//
//  AvaloqModal.swift
//  MyApp
//
//  Created by Bryner Decena on 12/3/19.
//

import UIKit

/*
 * Notes: The @objc shows that this class & function should be exposed to Cordova.
 */
@objc(AvaloqModal) class AvaloqModal : CDVPlugin {
    @objc(showModal:) // Declare your function name.
    func showModal(command: CDVInvokedUrlCommand) {
        
        //Get initial price passed from hybrid page
        let priceArr:NSArray = command.argument(at: 0) as! NSArray
        print("Swift Call \(priceArr[0])")
        let priceStr:String = priceArr[0] as! String
        
        //Get instance of page
        let topMostViewController = UIApplication.shared.keyWindow?.rootViewController

        //Show the modal in the view controller
        let storyBoard: UIStoryboard = UIStoryboard(name: "Dialog", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "NextViewController") as! NextViewController
        newViewController.modalPresentationStyle = .overCurrentContext
        //Pass data to the next view controller
        newViewController.price = priceStr
        newViewController.callbackId = command.callbackId
        newViewController.hybridView = self
        topMostViewController?.present(newViewController, animated: true, completion: nil)
    }
}
