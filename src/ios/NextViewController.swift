//
//  NextViewController.swift
//  testModal
//
//  Created by Bryner Decena on 12/3/19.
//  Copyright © 2019 Bryner Decena. All rights reserved.
//

import UIKit

class NextViewController: UIViewController {

    @IBOutlet weak var quantity: UITextField!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var btnPreview: UIButton!
    
    var callbackId:String = ""
    var price:String = ""
    var estChfAmount:Int = 0
    var hybridView:AvaloqModal? = nil
    var quantityEntered:String = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Only show the uiview and clear view controller
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        btnPreview.layer.cornerRadius = 10

        //Initialize values
        quantity.text = "1"
        amount.text = price
        
        //Initial Price Per Unit
        estChfAmount = Int(price)!
        
        
        //Dismiss number pad keyboard upon tap anywhere on view
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
    }
    
    @IBAction func close(_ sender: UIButton) {
        // Set the plugin result to fail.
        let pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "Do Not Change Anything");
        //Back to hybrid app
        backToHybrid(pluginResultValue: pluginResult!)
    }
    
    @IBAction func preview(_ sender: UIButton) {
        //Pass data to hybrid
        let estChfAmountBought = String(estChfAmount)
        print("estChfAmountBought: \(estChfAmountBought) callbackId: \(callbackId)")
        // Set the plugin result to succeed.
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: [quantityEntered,estChfAmountBought]);
        //Back to hybrid app
        backToHybrid(pluginResultValue: pluginResult!)
    }
    
    @IBAction func quantityFieldChanged(_ sender: UITextField) {
        print("quantityFieldChanged: \(String(describing: sender.text))")
        
        quantityEntered = sender.text!
        
        //Remove non-numeric characters
        quantityEntered = quantityEntered.filter("0123456789".contains)
        //Remove leading zeros
        quantityEntered = quantityEntered.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
        sender.text = quantityEntered
        
        if quantityEntered == "" {
            quantityEntered = "0"
        }
        //Display Est CHF Amount upon change in quantity
        estChfAmount = Int(quantityEntered)! * Int(price)!
        amount.text = "\(estChfAmount)"
        
    }
    
    func backToHybrid(pluginResultValue: CDVPluginResult) {
        // Send the function result back to Cordova.
        hybridView?.commandDelegate!.send(pluginResultValue, callbackId: callbackId);
        //Dismiss Modal
        self.dismiss(animated: true, completion: nil)
    }

}
